$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="popover"]').popover({ trigger: "hover" });
    $('.carousel').carousel({
        interval: 2000
    });
    $('#defaultModal').on('shown.bs.modal', function (e) {
        console.log('el modal se est� mostrando');
        $('#btnvestidos').removeClass('btn-primary');
        $('#btnvestidos').addClass('btn-outline-success');
        $('#btnvestidos').prop('disabled', true);
    });
    $('#defaultModal').on('shown.bs.modal', function (e) {
        console.log('el modal se mostr�');
    });
    $('#defaultModal').on('hide.bs.modal', function (e) {
        console.log('el modal se oculta');
    });
    $('#defaultModal').on('hidden.bs.modal', function (e) {
        console.log('el modal se ocult�');
        $('#btnvestidos').removeClass('btn-outline-success');
        $('#btnvestidos').addClass('btn-primary');
        $('#btnvestidos').prop('disabled', false);
    });

});